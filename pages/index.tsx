import React from 'react'
import { useQuery } from 'react-query';
import { getPosts } from './api/data';
import { VStack, Text, Spinner, useToast, Box } from '@chakra-ui/react';
import AddData from './components/addData';
import EditData from './components/editData';
import DeleteData from './components/deleteData';

export default function Home() {
  const { data: posts, isLoading, isError } = useQuery('posts', getPosts);

  if (isLoading) {
    return <Spinner size="xl" />;
  }

  if (isError) {
    return <Text>Error loading posts</Text>;
  }

  return (
    <VStack spacing={4} padding={10} align="stretch">
      <AddData />
      {posts.map((post: any) => (
        <Box key={post.id} borderWidth="1px" borderRadius="lg" overflow="hidden" bg="white" boxShadow="lg">
          <Box p="6">
            <Box alignItems="baseline">
              <Box
                color="gray.500"
                fontWeight="semibold"
                letterSpacing="wide"
                fontSize="xs"
                textTransform="uppercase"
                ml="2"
              >
                {post.category}
              </Box>
            </Box>

            <Box
              mt="1"
              fontWeight="semibold"
              as="h4"
              lineHeight="tight"
              isTruncated
            >
              {post.title}
            </Box>

            <Box>
              {post.body}
            </Box>

            <Box mt="2" alignItems="center">
              <EditData {...post} />
              <DeleteData {...post} />
            </Box>
          </Box>
        </Box>
      ))}
    </VStack>
  )
}
