import axios from 'axios';

export const getPosts = async () => {
    const { data } = await axios.get('https://jsonplaceholder.typicode.com/posts');
    return data;
};

export const getPostById = async (id: number) => {
  const { data } = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
  return data;
};

export const createPost = async (postData: any) => {
  const { data } = await axios.post(`https://jsonplaceholder.typicode.com/posts`, postData);
  return data;
};

export const updatePost = async (id: number, postData: any) => {
  const { data } = await axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, postData);
  return data;
};

export const deletePost = async (id: number) => {
  const { data } = await axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`);
  return data;
};
