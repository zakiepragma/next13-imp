import React, { useState } from 'react'
import { useMutation } from 'react-query';
import { useDisclosure, FormControl, FormErrorMessage, FormLabel, Input, Textarea, Button, useToast, Box, Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, Flex, Heading } from '@chakra-ui/react';
import { createPost } from '../api/data';

type Post = {
    title: string;
    body: string;
    userId: number;
};

export default function AddData() {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const [titleError, setTitleError] = useState('');
    const [bodyError, setBodyError] = useState('');

    const toast = useToast();

    const { mutate, isLoading, isError } = useMutation<any, any, any>(createPost, {
        onSuccess: (data) => {
            console.log(`Post created with id ${data.id}`);
            console.log(data);
            toast({
                title: "Post created",
                status: "success",
                duration: 3000,
                isClosable: true,
            });
            onClose();
        },
        onError: (error) => {
            toast({
                title: "Error post creating",
                status: "error",
                duration: 3000,
                isClosable: true,
            });
            console.error(`Error creating post: ${error.message}`);
        },
    });


    const handleCreatePost = () => {
        if (title === '') {
            setTitleError('Title is required');
            return;
        }
        if (body === '') {
            setBodyError('Body is required');
            return;
        }

        mutate({ title, body });
    };

    return (
        <>    <Flex justify="space-between" align="center">
            <Heading as="h1" size="md">POSTS</Heading>
            <Button colorScheme="blue" size="md" onClick={onOpen}>Add Post</Button>
        </Flex>

            <Modal isOpen={isOpen} onClose={onClose} size="xl">
                <ModalOverlay />
                <ModalContent borderRadius="md" mx={4}>
                    <ModalHeader fontWeight="bold" fontSize="2xl">Create Post</ModalHeader>
                    <ModalCloseButton />

                    <ModalBody>
                        <FormControl data-isinvalid={titleError}>
                            <FormLabel htmlFor="title">Title:</FormLabel>
                            <Input type="text" id="title" value={title} onChange={(e) => setTitle(e.target.value)} />
                            <FormErrorMessage>{titleError}</FormErrorMessage>
                        </FormControl>
                        <FormControl data-isinvalid={bodyError}>
                            <FormLabel htmlFor="body">Content:</FormLabel>
                            <Textarea id="content" value={body} onChange={(e) => setBody(e.target.value)} />
                            <FormErrorMessage>{bodyError}</FormErrorMessage>
                        </FormControl>
                    </ModalBody>

                    <Box py={4} px={6}>
                        <Button colorScheme="blue" borderRadius="md" onClick={handleCreatePost}>Create Post</Button>
                    </Box>
                </ModalContent>
            </Modal>
        </>

    )
}
