import { Button, useToast, Modal, ModalOverlay, ModalContent, ModalHeader, ModalBody, ModalFooter } from '@chakra-ui/react'
import React, { useState } from 'react'
import { useMutation } from 'react-query';
import { deletePost } from '../api/data';

type Post = {
    id: number;
    title: string;
    body: string
}

export default function DeleteData(post: Post) {

    const toast = useToast();
    const [showModal, setShowModal] = useState(false);

    const { mutate, isLoading, isError } = useMutation<any, any, any>(deletePost, {
        onSuccess: (data) => {
            console.log(`Post deleted`);
        },
        onError: (error) => {
            console.error(`Error deleted post`);
        },
    });

    const handleDelete = (postId: number) => {
        mutate(postId);
        toast({
            title: "Post deleted",
            status: "success",
            duration: 3000,
            isClosable: true,
        });
    }

    const handleShowModal = () => setShowModal(true);
    const handleHideModal = () => setShowModal(false);

    return (
        <>
            <Button colorScheme="red" onClick={handleShowModal}>Delete</Button>
            <Modal isOpen={showModal} onClose={handleHideModal}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Confirmation</ModalHeader>
                    <ModalBody>
                        Are you sure you want to delete this post ({post.title})?
                    </ModalBody>
                    <ModalFooter>
                        <Button colorScheme="red" mr={3} onClick={() => {
                            handleDelete(Number(post.id));
                            handleHideModal();
                        }}>
                            Delete
                        </Button>
                        <Button variant="ghost" onClick={handleHideModal}>Cancel</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}