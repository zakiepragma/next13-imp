import { Button, Modal, ModalOverlay, ModalContent, ModalHeader, ModalBody, ModalFooter, FormControl, FormLabel, Input, useToast, FormErrorMessage } from '@chakra-ui/react'
import React, { useState } from 'react'
import { useMutation } from 'react-query';
import { updatePost } from '../api/data';

type Post = {
    id: number;
    title: string;
    body: string
}

export default function EditData(post: Post) {
    const [showModal, setShowModal] = useState(false);
    const [title, setTitle] = useState(post.title);
    const [body, setBody] = useState(post.body);

    const [titleError, setTitleError] = useState('');
    const [bodyError, setBodyError] = useState('');

    const handleShowModal = () => setShowModal(true);
    const handleHideModal = () => setShowModal(false);

    const toast = useToast();

    const { mutate, isLoading, isError } = useMutation<any, any, { id: number, data: { title: string, body: string } }, any>(
        ({ id, data }) => updatePost(id, data),
        {
            onSuccess: (data) => {
                console.log(`Post updated with id ${data.id}`);
                console.log(data)
                toast({
                    title: "Post updated",
                    status: "success",
                    duration: 3000,
                    isClosable: true,
                });
                handleHideModal();
            },
            onError: (error) => {
                toast({
                    title: "Error post updating",
                    status: "error",
                    duration: 3000,
                    isClosable: true,
                });
                console.error(`Error updating post: ${error.message}`);
            },
        }
    );

    const handleSubmit = () => {
        if (title === '') {
            setTitleError('Title is required');
            return;
        }
        if (body === '') {
            setBodyError('Body is required');
            return;
        }

        mutate({ id: post.id, data: { title, body } });
    };

    return (
        <>
            <Button colorScheme="green" mr="2" onClick={handleShowModal}>Edit</Button>
            <Modal isOpen={showModal} onClose={handleHideModal}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Edit Data</ModalHeader>
                    <ModalBody>
                        <FormControl>
                            <FormLabel>Title</FormLabel>
                            <Input type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
                            <FormErrorMessage>{titleError}</FormErrorMessage>
                        </FormControl>
                        <FormControl mt={4}>
                            <FormLabel>Body</FormLabel>
                            <Input type="text" value={body} onChange={(e) => setBody(e.target.value)} />
                            <FormErrorMessage>{bodyError}</FormErrorMessage>
                        </FormControl>
                    </ModalBody>
                    <ModalFooter>
                        <Button colorScheme="green" mr={3} onClick={handleSubmit}>Save</Button>
                        <Button variant="ghost" onClick={handleHideModal}>Cancel</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}
